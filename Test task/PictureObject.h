//
//  PictureObject.h
//  Test task
//
//  Created by Oleg Mytsovda on 19.08.17.
//  Copyright © 2017 Oleg Mytsovda. All rights reserved.
//

#import <Realm/Realm.h>

@interface PictureObject : RLMObject

@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *imageURL;

+ (NSString *)primaryKey;

@end
