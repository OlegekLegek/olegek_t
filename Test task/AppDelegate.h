//
//  AppDelegate.h
//  Test task
//
//  Created by Oleg Mytsovda on 19.08.17.
//  Copyright © 2017 Oleg Mytsovda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

