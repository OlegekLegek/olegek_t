//
//  AddingCell.m
//  Test task
//
//  Created by Oleg Mytsovda on 20.08.17.
//  Copyright © 2017 Oleg Mytsovda. All rights reserved.
//

#import "AddingCell.h"

@implementation AddingCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.translatesAutoresizingMaskIntoConstraints = false;
        self.addValueButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44)];
        [self.addValueButton addTarget:self.delegate action:@selector(addingValue:) forControlEvents:UIControlEventTouchUpInside];
        [self.addValueButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self addSubview:self.addValueButton];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

}

@end
