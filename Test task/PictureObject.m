//
//  PictureObject.m
//  Test task
//
//  Created by Oleg Mytsovda on 19.08.17.
//  Copyright © 2017 Oleg Mytsovda. All rights reserved.
//

#import "PictureObject.h"

@implementation PictureObject

+ (NSString *)primaryKey
{
    return @"imageURL";
}

@end
