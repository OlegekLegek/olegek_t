//
//  AddingValueCell.h
//  Test task
//
//  Created by Oleg Mytsovda on 20.08.17.
//  Copyright © 2017 Oleg Mytsovda. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddingDelegate <NSObject>

- (void)addingValue;

@end

@interface AddingValueCell : UITableViewCell

@property (weak, nonatomic) id<AddingDelegate> delegate;
@property (strong, nonatomic) UIButton *addValueButton;

@end
