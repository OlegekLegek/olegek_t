//
//  PhotoCell.h
//  Test task
//
//  Created by Oleg Mytsovda on 21.08.17.
//  Copyright © 2017 Oleg Mytsovda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PictureObject.h"

@interface PhotoCell : UITableViewCell

@property (strong, nonatomic) UIImageView *photoImage;

@property (strong, nonatomic) PictureObject *picture;

@end
