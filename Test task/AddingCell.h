//
//  AddingCell.h
//  Test task
//
//  Created by Oleg Mytsovda on 20.08.17.
//  Copyright © 2017 Oleg Mytsovda. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AddingCell;

@protocol AddingDelegate <NSObject>

- (void)addingValue:(UIButton *)sender;

@end

@interface AddingCell : UITableViewCell

@property (weak, nonatomic) id<AddingDelegate> delegate;

@property (strong, nonatomic) UIButton *addValueButton;
@property (strong, nonatomic) NSString *type;

@end
