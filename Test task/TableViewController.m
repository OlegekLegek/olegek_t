//
//  TableViewController.m
//  Test task
//
//  Created by Oleg Mytsovda on 19.08.17.
//  Copyright © 2017 Oleg Mytsovda. All rights reserved.
//

#import "TableViewController.h"
#import "PictureObject.h"
#import "AddingCell.h"
#import "PhotoCell.h"

@interface TableViewController () <AddingDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) NSMutableArray *userIdPhotos;
@property (strong, nonatomic) NSMutableArray *certeficatePhotos;

@property (strong, nonatomic) PictureObject *currentObject;

@end

@implementation TableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self parsingValues];
    self.navigationItem.title = @"Test Task";
    [self.tableView registerClass:[AddingCell class] forCellReuseIdentifier:@"add"];
    [self.tableView registerClass:[PhotoCell class] forCellReuseIdentifier:@"photo"];
}

#pragma mark - Parse Value

- (void)parsingValues
{
    [self initArraysWithCustomValues];
    for (PictureObject *obj in [self getValueFromDB]) {
        if ([self checkForUserIdPhotos:obj.type]) {
            [self.userIdPhotos addObject:obj];
        } else {
            [self.certeficatePhotos addObject:obj];
        }
    }
    [self.tableView reloadData];
}

- (void)initArraysWithCustomValues
{
    self.userIdPhotos = [[NSMutableArray alloc] initWithObjects:@"add", nil];
    self.certeficatePhotos = [[NSMutableArray alloc] initWithObjects:@"add", nil];;
}

- (BOOL)checkForUserIdPhotos:(NSString *)type
{
    return [type isEqualToString:@"user"];
}

- (RLMResults *)getValueFromDB
{
    return [PictureObject allObjects];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return section == 0 ? @"User ID Photos" : @"Certificate Photos";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return self.userIdPhotos.count;
    } else {
        return self.certeficatePhotos.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableArray *currentValues = [self returnCurrentValuesFromSection:(int)indexPath.section];
    if ([currentValues[indexPath.row] isKindOfClass:[NSString class]]) {
        return [self generateAddingCellWithTable:tableView Title: [self returnTitleFromSection:(int)indexPath.section] andTag:(int)indexPath.section];
    } else {
        return [self generatePhotoCellWithTable:tableView andPictureObject:currentValues[indexPath.row]];
    }
}

- (NSMutableArray *)returnCurrentValuesFromSection:(int)section
{
    return section == 0 ? self.userIdPhotos : self.certeficatePhotos;
}


- (NSString *)returnTitleFromSection:(int)section
{
    return section == 0 ? @"Add User Photos" : @"Add Certeficate Photos";
}

- (AddingCell *)generateAddingCellWithTable:(UITableView *)tableView Title:(NSString *)title andTag:(int) tag
{
    AddingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"add"];
    [cell.addValueButton setTitle:title forState:UIControlStateNormal];
    cell.addValueButton.tag = tag;
    cell.delegate = self;
    return cell;
}

- (PhotoCell *)generatePhotoCellWithTable:(UITableView *)tableView andPictureObject:(PictureObject *)picture
{
    PhotoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"photo"];
    cell.photoImage.image = [self loadImageWithName:picture.imageURL];
    cell.picture = picture;
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return NO;
    } else {
        return YES;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        PhotoCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        [self remove:cell.picture];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 44;
    } else {
        return 120;
    }
}

#pragma mark - Get and Adding Value to DB

- (void)addingValue:(UIButton *)sender
{
    self.currentObject = [PictureObject new];
    self.currentObject.type = sender.tag == 0 ? @"user" : @"certeficate";
    [self showPicker];
}

- (void)showPicker
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Chose Action" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* cameraAction = [UIAlertAction actionWithTitle:@"Take Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self takePhoto];
    }];
    
    UIAlertAction* galleryAction = [UIAlertAction actionWithTitle:@"Chose From Galery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self selectPhotos];
    }];
    
    UIAlertAction * defaultAct = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
        self.currentObject = nil;
    }];
    
    [alert addAction:cameraAction];
    [alert addAction:galleryAction];
    [alert addAction:defaultAct];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)selectPhotos
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)takePhoto
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    [self dismissViewControllerAnimated:YES completion:NULL];
    UIImage *takenPhoto = [info objectForKey:UIImagePickerControllerOriginalImage];
    NSURL* imageUrl = [info valueForKey:UIImagePickerControllerReferenceURL];
    [self saveImage:takenPhoto withName:[imageUrl absoluteString]];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Local Operation

- (void)remove:(PictureObject *)picture
{
    [self removeImageWithName:picture.imageURL];
    [self removeObjectFromDB:picture];
    [self parsingValues];
}

- (void)saveImage:(UIImage *)image withName:(NSString *)name
{
    NSData *imageData = UIImagePNGRepresentation(image);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *photoName = [[NSUUID UUID] UUIDString];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.PNG",photoName]];
    [imageData writeToFile:path atomically:YES];
    self.currentObject.imageURL = photoName;
    [self addObjectToDB];
}

- (UIImage*)loadImageWithName:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.PNG", name]];

    UIImage* image = [UIImage imageWithContentsOfFile:path];
    return image;
}

- (void)removeImageWithName:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.PNG", name]];
    NSError *error;
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        if (![[NSFileManager defaultManager] removeItemAtPath:path error:&error]) {
            NSLog(@"Delete file error: %@", error);
        }
    }
}

#pragma mark - DB Operations

- (void)addObjectToDB
{
    [[RLMRealm defaultRealm] beginWriteTransaction];
    [[RLMRealm defaultRealm] addObject:self.currentObject];
    [[RLMRealm defaultRealm] commitWriteTransaction];
    self.currentObject = nil;
    [self parsingValues];
}

- (void)removeObjectFromDB:(PictureObject *)picture
{
    [[RLMRealm defaultRealm] beginWriteTransaction];
    [[RLMRealm defaultRealm] deleteObject:picture];
    [[RLMRealm defaultRealm] commitWriteTransaction];
    [self parsingValues];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
